# OptimaxGrid

## Der Algorithmus

Direkt zum Quellcode des Algorithmus geht es unter `src/classes/Grid.ts`.

Der Algorithmus arbeitet mit einem zweidimensionalen Array (Cells) von Zahlen, die die Belegung der Zellen beschreiben (0 = frei, 1 = besetzt). 
Damit ein Grid angelegt werden kann, muss zunächst die statische Methode Grid.createGrid aufgerufen werden, die das Grid überprüft und entweder ein 
neues Grid oder bei falschen Eingaben null zurückgibt.

Soll ein neues Element hinzugefügt werden, prüft der Algorithmus bestimmte Positionen im Grid auf Kollisionen. 
Das bedeutet: Er prüft, ob eine der Zellen, die das neue Element in dieser Position benötigen würde, besetzt ist.
Gibt es eine Kollision, speichert er den Kollisionspunkt (Punkt, den das Element an der Position benötigt hätte, der jedoch schon besetzt ist) 
in einer Liste.
Um die nächste Position zu ermitteln, an der versucht werden kann, das Element einzufügen, wird nun die Kollisionspunkt-Liste verwendet.
Liegt auf einer Position im Bereich des Elementes ein Kollisionspunkt, kann die Position übersprungen werden.
Es werden immer die am weitesten oben (bzw links) liegenden Positionen ausgewählt.

Das Arbeiten mit der Kollisionspunkt-Liste sorgt dafür, dass beim Suchen nach der optimalen Position möglichst viele Positionen übersprungen 
werden können.
Das zahlt sich vor Allem bei großen Elementen aus, da sie aufgrund ihrer Breite auf viele Kollisionspunkte treffen und die entsprechenden 
Positionen überspringen können.


## Das Projekt

Das Projekt wurde mit [Angular CLI](https://github.com/angular/angular-cli) Version 7.3.3 erstellt. 
Zum Ausführen und Testen muss es zunächst geklont oder herunterladen werden und  `npm install` im Ordner ausgeführt werden.
Anschließen gibt es folgende Optionen:

### Unittests starten

mit `npm run test`, die Tests werden mit [Karma](https://karma-runner.github.io) ausgeführt.


### Development Server starten

mit `ng serve`, auf `http://localhost:4200/` steht anschließend das Frontend bereit.


