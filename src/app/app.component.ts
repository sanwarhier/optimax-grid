import { Component } from '@angular/core';
import { Grid } from '../classes/Grid';
import { GridElement } from '../classes/GridElement';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {

  private _Grid: Grid;
  public get Grid(): Grid {
    return this._Grid;
  }
  public set Grid(v: Grid) {
    this._Grid = v;
  }

  private _GridElements: GridElement[];
  public get GridElements(): GridElement[] {
    return this._GridElements;
  }
  public set GridElements(v: GridElement[]) {
    this._GridElements = v;
  }

  private _ElementColors: Map<GridElement, string>;
  public get ElementColors(): Map<GridElement, string> {
    return this._ElementColors;
  }
  public set ElementColors(v: Map<GridElement, string>) {
    this._ElementColors = v;
  }

  private style: Partial<CSSStyleDeclaration>;
  public get Style(): Partial<CSSStyleDeclaration> {
    return this.style;
  }
  public set Style(v: Partial<CSSStyleDeclaration>) {
    this.style = v;
  }

  private _Rows: number[];
  public get Rows(): number[] {
    return this._Rows;
  }
  public set Rows(v: number[]) {
    this._Rows = v;
  }

  /**
   * Konstruktor für AppComponent
   */
  constructor() {
    // Elemente, die beim Start der Seite erscheinen sollen:
    this.GridElements = [
      new GridElement({ Height: 3, Width: 2 }, { X: 3, Y: 2 }),
      new GridElement({ Height: 2, Width: 2 }, { X: 0, Y: 4 })
    ];

    // initiale Elemente einfärben:
    this.ElementColors = new Map();
    for (const element of this.GridElements) {
      this.ElementColors.set(element, this.getRandomColor());
    }

    // Grid erstellen:
    this.Grid = Grid.createGrid({ Width: 8, Height: 10 }, this.GridElements);
  }

  /**
   * Wird aufgerufen, wenn auf den Button 'Hinzufügen' geklickt wird.
   * Fügt ein neues Element mit den Werten aus den Inputs hinzu
   * @param height Wert aus dem Höhe-Input
   * @param width Wert aus dem Breite-Input
   */
  public onClickAddElement(height: number, width: number) {
    height = +height; // sicherstellen, dass typeof(height)==number
    width = +width;
    const position = this.Grid.addElement({ Height: height, Width: width });
    if (position) {
      const newGridElement = new GridElement({ Height: height, Width: width }, position);
      this.GridElements.push(newGridElement);
      this.ElementColors.set(newGridElement, this.getRandomColor());
    } else {
      console.log('Element zu groß oder zu klein');
    }
  }

  /**
   * Wird aufgerufen, wenn auf den Button 'Erstellen' geklickt wird.
   * @param height Wert aus dem Höhe-Input
   * @param width Wert aus dem Breite-Input
   */
  public onClickCreateGrid(height: number, width: number) {
    height = +height; // sicherstellen, dass typeof(height)==number
    width = +width;
    const grid = Grid.createGrid({ Height: height, Width: width }, []);
    if (grid) {
      this.Grid = grid;
      this.GridElements = [];
    }
  }

  /**
   * generiert eine zufällige Farbe (ohne weiß)
   * @returns zufällig generierte Farbe
   */
  public getRandomColor(): string {
    const hexLetters = '0123456789ABCDE'; // F ausgeschlossen um weiß zu verhindern
    let color = '#';
    for (let i = 0; i < 6; i++) {
      color += hexLetters[Math.floor(Math.random() * 15)];
    }
    return color;
  }

  /**
   * gibt die Zeilennummerierungen als Array zurück
   * @returns Zeilennummern
   */
  public getRowNumbers(): number[] {
    const numbers = [];
    for (let i = 0; i < this.Grid.getHeight(); i++) {
      numbers.push(i);
    }
    return numbers;
  }

  /**
   * gibt die Spaltennummerierungen als Array zurück
   * @returns Spaltennummern
   */
  public getColNumbers(): number[] {
    const numbers = [];
    for (let i = 0; i < this.Grid.getWidth(); i++) {
      numbers.push(i);
    }
    return numbers;
  }
}
