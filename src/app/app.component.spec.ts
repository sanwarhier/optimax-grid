import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Grid } from '../classes/Grid';
import { GridElement } from '../classes/GridElement';
import { IPosition } from '../interfaces/IPosition';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});


describe('addElement on Grid 1', () => {
  const testGrid = Grid.createGrid({ Width: 16, Height: 8 }, [
    new GridElement({ Width: 6, Height: 3 }, { X: 0, Y: 0 }),
    new GridElement({ Width: 6, Height: 6 }, { X: 6, Y: 0 })
  ]);

  it('should return (0, 3)', () => {
    const newEl = { Width: 5, Height: 3 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 3 });
  });
});

describe('addElement on Grid 2', () => {
  const testGrid = Grid.createGrid({ Width: 10, Height: 7 }, [
    new GridElement({ Width: 2, Height: 2 }, { X: 6, Y: 0 }),
    new GridElement({ Width: 2, Height: 2 }, { X: 2, Y: 2 })
  ]);

  it('should return (4, 2)', () => {
    const newEl = { Width: 4, Height: 4 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 4, Y: 2 });
  });

  it('should return (0, 0)', () => {
    const newEl = { Width: 6, Height: 2 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 0 });
  });

  it('should return (0, 6)', () => {
    const newEl = { Width: 6, Height: 20 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 6 });
  });

  it('should return null', () => {
    const newEl = { Width: 70, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual(null);
  });

  it('should return null ', () => {
    const newEl = { Width: 12, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual(null);
  });

  it('should return (0, 26) ', () => {
    const newEl = { Width: 10, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 26 });
  });
});

/**
 * Testfälle in denen die Höhe vergrößert werden muss:
 */

describe('addElement on Grid 3', () => {
  const testGrid = Grid.createGrid({ Width: 7, Height: 4 }, [
    new GridElement({ Width: 2, Height: 3 }, { X: 1, Y: 0 }),
    new GridElement({ Width: 2, Height: 3 }, { X: 5, Y: 1 })
  ]);

  it('should return (0, 3)', () => {
    const newEl = { Width: 5, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 3 });
  });

  it('should return (0, 4)', () => {
    const newEl = { Width: 5, Height: 2 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 4 });
  });

  it('should return (0, 6)', () => {
    const newEl = { Width: 5, Height: 3 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 6 });
  });
});

describe('addElement on Grid 4', () => {
  const testGrid = Grid.createGrid({ Width: 4, Height: 3 }, [
    new GridElement({ Width: 4, Height: 3 }, { X: 0, Y: 0 }),
  ]);

  it('should return (0, 3)', () => {
    const newEl = { Width: 3, Height: 2 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 3 });
  });
});

describe('addElement on Grid 5', () => {
  const testGrid = Grid.createGrid({ Width: 4, Height: 3 }, [
    new GridElement({ Width: 4, Height: 3 }, { X: 0, Y: 0 }),
  ]);

  it('should return (0, 3)', () => {
    const newEl = { Width: 3, Height: 2 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 3 });
  });
});

describe('addElement on Grid 6', () => {
  const testGrid = Grid.createGrid({ Width: 10, Height: 10 }, [
    new GridElement({ Height: 3, Width: 2 }, { X: 3, Y: 4 }),
  ]);

  it('should return (0, 0)', () => {
    const newEl = { Width: 2, Height: 2 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 0 });
  });

  it('should return (2, 0)', () => {
    const newEl = { Width: 2, Height: 2 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 2, Y: 0 });
  });
});

describe('addElement on Grid 7', () => {
  const testGrid = Grid.createGrid({ Width: 10, Height: 10 }, []);

  it('should return (0, 0)', () => {
    const newEl = { Width: 1, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 0, Y: 0 });
  });

  it('should return (1, 0)', () => {
    const newEl = { Width: 1, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 1, Y: 0 });
  });

  it('should return (2, 0)', () => {
    const newEl = { Width: 1, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 2, Y: 0 });
  });

  it('should return (3, 0)', () => {
    const newEl = { Width: 1, Height: 1 };
    let newPosition = null;
    if (testGrid) {
      newPosition = testGrid.addElement(newEl);
    }
    expect(newPosition).toEqual({ X: 3, Y: 0 });
  });
});


/**
 * Höhe des Grids testen
 */
describe('Height of Grid', () => {
  const testGrid = Grid.createGrid({ Width: 1, Height: 1 }, []);

  it('should be 1', () => {
    const newEl = { Width: 1, Height: 1 };
    testGrid.addElement(newEl);
    const height = testGrid.getHeight();
    expect(height).toEqual(1);
  });

  it('should be 2', () => {
    const newEl = { Width: 1, Height: 1 };
    testGrid.addElement(newEl);
    const height = testGrid.getHeight();
    expect(height).toEqual(2);
  });
});



/**
 * Grid erstellen
 */

/**
 * Testfälle für: Grid ist zu klein
 */
describe('createGrid when Grid is too small', () => {
  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: -9, Height: 8 }, []);
    expect(testGrid).toEqual(null);
  });

  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 29, Height: -1 }, []);
    expect(testGrid).toEqual(null);
  });

  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 0, Height: 1 }, []);
    expect(testGrid).toEqual(null);
  });

  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 0, Height: -1 }, []);
    expect(testGrid).toEqual(null);
  });

  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: -4, Height: -1 }, []);
    expect(testGrid).toEqual(null);
  });

  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 0, Height: 0 }, []);
    expect(testGrid).toEqual(null);
  });
});

/**
 * Testfälle für: Komponente ist zu klein
 */
describe('createGrid when one GridElement is too small', () => {
  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 4, Height: 4 }, [
      new GridElement({ Height: 2, Width: -1 }, { X: 0, Y: 0 })
    ]);
    expect(testGrid).toEqual(null);
  });
  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 4, Height: 4 }, [
      new GridElement({ Height: 0, Width: 2 }, { X: 0, Y: 0 })
    ]);
    expect(testGrid).toEqual(null);
  });
  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 4, Height: 4 }, [
      new GridElement({ Height: -2, Width: 0 }, { X: 0, Y: 0 })
    ]);
    expect(testGrid).toEqual(null);
  });
});

/**
 * Testfälle für: Komponente ist nicht vollständig im Grid
 */
describe('createGrid when one GridElement is not in Grid', () => {
  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 4, Height: 4 }, [
      new GridElement({ Height: 2, Width: 5 }, { X: 0, Y: 0 })
    ]);
    expect(testGrid).toEqual(null);
  });
  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 4, Height: 4 }, [
      new GridElement({ Height: 3, Width: 4 }, { X: 1, Y: 1 })
    ]);
    expect(testGrid).toEqual(null);
  });
  it('should return null', () => {
    const testGrid = Grid.createGrid({ Width: 4, Height: 4 }, [
      new GridElement({ Height: 2, Width: 2 }, { X: 8, Y: 12 })
    ]);
    expect(testGrid).toEqual(null);
  });
});


/**
 * Methode getNextXPosition
 */

// von Grid erben um privtate Methode zu testen:
class TestGrid extends Grid {
  constructor() {
    super({ Width: 1, Height: 1 }, []);
  }
  public testGetNextXPosition(collisionPoints: IPosition[], col: number, elementWidth: number) {
    return this.getNextXPosition(collisionPoints, col, elementWidth);
  }
}

describe('getNextXPosition on CollisionPoint List 1', () => {
  const testGrid = new TestGrid();

  const testCollisionPoints = [
    { X: 2, Y: 6 },
    { X: 5, Y: 4 },
    { X: 9, Y: 7 }
  ];

  it('should return 1 for width = 1, col = 0', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, 0, 1);
    expect(xPosition).toEqual(1);
  });

  it('should return 3 for width = 2 col = 0;', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, 0, 2);
    expect(xPosition).toEqual(3);
  });

  it('should return 6 for width = 3 col = 0;', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, 0, 3);
    expect(xPosition).toEqual(6);
  });

  it('should return 3 for width = 1, col = 2', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, 2, 1);
    expect(xPosition).toEqual(3);
  });

  it('should return 3 for width = 2 col = 2;', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, 2, 2);
    expect(xPosition).toEqual(3);
  });

  it('should return 6 for width = 3 col = 2;', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, 2, 3);
    expect(xPosition).toEqual(6);
  });
});


describe('getNextXPosition on CollisionPoint List 2', () => {
  const testGrid = new TestGrid();

  const testCollisionPoints = [
    { X: 2, Y: 6 },
    { X: 5, Y: 4 },
    { X: 9, Y: 7 },
    { X: 3, Y: 5 }
  ];
  const col = 0;

  it('should return 1 for width = 1', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, col, 1);
    expect(xPosition).toEqual(1);
  });

  it('should return 6 for width = 2', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, col, 2);
    expect(xPosition).toEqual(6);
  });

  it('should return 10 for width = 4', () => {
    const xPosition = testGrid.testGetNextXPosition(testCollisionPoints, col, 4);
    expect(xPosition).toEqual(10);
  });
});
