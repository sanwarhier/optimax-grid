export interface ISize {
  readonly Width: number;
  Height: number;
}
