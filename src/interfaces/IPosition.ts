export interface IPosition {
  readonly X: number;
  readonly Y: number;
}
