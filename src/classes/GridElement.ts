import { IPosition } from '../interfaces/IPosition';
import { ISize } from '../interfaces/ISize';

/**
 * Element mit Position im Grid
 */
export class GridElement implements ISize, IPosition {
  protected readonly width: number;
  public get Width() {
    return this.width;
  }

  protected readonly height: number;
  public get Height() {
    return this.height;
  }

  protected readonly x: number;
  public get X() {
    return this.x;
  }

  protected readonly y: number;
  public get Y() {
    return this.y;
  }

  constructor(size: ISize, position: IPosition) {
    this.width = size.Width;
    this.height = size.Height;
    this.x = position.X;
    this.y = position.Y;
  }
}
