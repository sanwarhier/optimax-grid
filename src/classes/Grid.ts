import { ISize } from '../interfaces/ISize';
import { IPosition } from '../interfaces/IPosition';
import { GridElement } from '././GridElement';

/**
 * 2-Dimensionales Grid
 */
export class Grid {
  protected _Cells: number[][];
  public get Cells(): number[][] {
    return this._Cells;
  }
  public set Cells(v: number[][]) {
    this._Cells = v;
  }

  /**
   * Grid-Konstruktor
   * @param size Breite und Höhe des Grids
   * @param gridElements initiale Elemente
   */
  protected constructor(size: ISize, gridElements: GridElement[]) {
    this.Cells = [];
    this.init(size);
    this.fill(gridElements);
  }

  /**
   * Gibt ein Grid zurück, wenn Parameter gültig sind
   * @param size Höhe und Breite des Grids
   * @param gridElements Elemente des initialen Grids (müssen nicht optimal positioniert sein)
   */
  public static createGrid(size: ISize, gridElements: GridElement[]): Grid | null {
    if (size.Height <= 0 || size.Width <= 0) {
      return null;
    }
    for (const element of gridElements) {
      // Element zu klein:
      if (element.Width <= 0 || element.Height <= 0) {
        return null;
      }
      // Position im negativen Bereich:
      if (element.X < 0 || element.Y < 0) {
        return null;
      }
      // Element zu groß:
      if (element.X + element.Width > size.Width) {
        return null;
      }
    }
    return new Grid(size, gridElements);
  }

  /**
   * Fügt das übergebene Element an der optimalen Position hinzu
   * @param elSize Größe des neuen Elements
   * @returns optimale Position oder null
   */
  public addElement(elSize: ISize): IPosition | null {
    const position = this.getBestPosition(elSize);
    if (position) {
      this.reserveCells(new GridElement(elSize, position));
      return position;
    }
    return null;
  }

  /**
   * Gibt die Höhe des Grids zurück
   * @returns Höhe des Grids (Cell[0].length)
   */
  public getHeight(): number {
    return this.Cells[0].length;
  }

  /**
   * Gibt die Breite des Grids zurück
   * @returns Breite des Grids (Cells.length)
   */
  public getWidth(): number {
    return this.Cells.length;
  }

  /**
   * erstellt leeres Grid
   */
  protected init(gridSize: ISize): void {
    for (let colNr = 0; colNr < gridSize.Width; colNr++) {
      this.Cells[colNr] = [];
      for (let rowNr = 0; rowNr < gridSize.Height; rowNr++) {
        this.Cells[colNr][rowNr] = 0;
      }
    }
  }

  /**
   * belegt die Zellen des Grids (Cells)
   */
  protected fill(elements: GridElement[]): void {
    for (const comp of elements) {
      // Bei Bedarf neue Zeilen hinzufügen:
      if (comp.Y + comp.Height > this.getHeight()) {
        this.addRows(comp.Y + comp.Height - this.getHeight());
      }
      // Element eintragen:
      for (let colNr = 0; colNr < comp.Width; colNr++) {
        for (let rowNr = 0; rowNr < comp.Height; rowNr++) {
          this.Cells[colNr + comp.X][rowNr + comp.Y] = 1;
        }
      }
    }
  }

  /**
   * speichert neues Element in Cells
   * @param element neues Element
   */
  protected reserveCells(element: GridElement): void {
    const x = element.X;
    const y = element.Y;
    for (let colNr = 0; colNr < element.Width; colNr++) {
      for (let rowNr = 0; rowNr < element.Height; rowNr++) {
        this.Cells[colNr + x][rowNr + y] = 1;
      }
    }
  }

  /**
   * Vergrößert die Höhe des Grids um nrOfRows
   * @param nrOfRows Anzahl der Zeilen, die hinzugefügt werden sollen
   */
  protected addRows(nrOfRows: number): void {
    for (let colNr = 0; colNr < this.getWidth(); colNr++) {
      for (let i = 0; i < nrOfRows; i++) {
        this.Cells[colNr].push(0);
      }
    }
  }

  /**
   * Fügt dem Grid ein neues Element hinzu;
   * Reicht der Platz nicht aus, wird das Grid vergrößert
   * @param newElement Element, das hinzugefügt werden soll
   * @returns GridElement mit bester Position
   */
  protected getBestPosition(newElement: ISize): IPosition | null {
    // zu breites Element:
    if (newElement.Width > this.getWidth()) {
      return null;
    }
    let collisionPoints: IPosition[] = [];
    for (let rowNr = 0; rowNr < this.getHeight(); rowNr++) {
      for (let colNr = 0; colNr <= (this.getWidth() - newElement.Width); colNr++) { // letzte newElement.width Zellen auslassen
        // ersten Kollisionspunkt an aktueller Position ermitteln:
        const currentPosition: IPosition = { X: colNr, Y: rowNr };
        const collisionPoint = this.checkForCollision(currentPosition, newElement);
        if (collisionPoint) {
          collisionPoints.push(collisionPoint);
        } else {  // Optimale Position gefunden
          return currentPosition;
        }
        colNr = this.getNextXPosition(collisionPoints, colNr, newElement.Width) - 1;
      }
      // Kollisionspunkte bis zur nächsten Zeile löschen:
      collisionPoints = collisionPoints.filter(point => point.Y > rowNr);
      // Höhe vergrößern bei Bedarf:
      if (rowNr === (this.getHeight() - 1)) {
        this.addRows(1);
      }
    }
    return null;
  }

  /**
   * Prüft ob Zahl (number) zwischen min und max liegt (min und max eingeschlossen)
   * @param number zu prüfende Zahl
   * @param min untere Schranke
   * @param max obere Schranke
   */
  protected isInRange(nr: number, min: number, max: number): boolean {
    return (min <= nr && nr <= max);
  }

  /**
   * Berechnet die nächste Position, auf der es keine bekannten Kollisionen gibt
   * @param collisionPoints Liste der ermittelten Kollisionspunkte
   * @param col aktuelle Spaltennummer
   * @param elementWidth Breite des einzufügenden Elements
   */
  protected getNextXPosition(collisionPoints: IPosition[], col: number, elementWidth: number): number {
    let newX = col + 1;
    let positionIncreased = true;
    while (positionIncreased) {
      positionIncreased = false;
      for (const collisionPoint of collisionPoints) {
        if (this.isInRange(collisionPoint.X, newX, (newX + elementWidth - 1))) {
          newX = collisionPoint.X + 1;
          positionIncreased = true;
        } else {
        }
      }
    }
    return newX;
  }

  /**
   * prüft, ob das Element an der bestimmten Stelle (insertPoint) eingefügt werden kann;
   * wenn nicht, wird der erste gefundene Kollisionspunkt zurückgegeben
   * @param insertPoint Position an der das Element hinzugefügt werden soll
   * @param el einzufügendes Element
   * @returns Kollisionspunkt oder null
   */
  protected checkForCollision(insertPoint: IPosition, el: ISize): IPosition | null {
    if (insertPoint.Y + el.Height > this.getHeight()) {
      this.addRows(el.Height);
    }
    const lowerBound = insertPoint.Y + el.Height;
    const rightBound = insertPoint.X + el.Width;
    for (let rowNr = insertPoint.Y; rowNr < lowerBound; rowNr++) {
      for (let colNr = insertPoint.X; colNr < rightBound; colNr++) {
        if (this.Cells[colNr][rowNr]) {
          return { X: colNr, Y: rowNr };  // Kollisionspunkt zurückgeben
        }
      }
    }
    return null;  // Falls keine Kollision auftritt
  }
}
